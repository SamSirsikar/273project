from flask import Flask
import os

app = Flask(__name__)

@app.route("/app4/hello")
def hello():
    return "Hellooooooooo App4!"

if __name__ == "__main__":
    host = os.getenv('IP', '0.0.0.0')
    port = os.getenv('PORT', 8080)
    app.run(host=host,port=int(port))
